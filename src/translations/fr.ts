import { Base, Size } from '../models/type/type';

export default {
  base: {
    [Base.TOMATO]: 'tomate',
    [Base.CREAM]: 'crème',
  },
  size: {
    [Size.SMALL]: 'petite',
    [Size.MEDIUM]: 'medium',
    [Size.LARGE]: 'large',
  },
};
