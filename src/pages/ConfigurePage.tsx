import { useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';

import emptyBase from '../assets/images/empty.png';
import RadioSelection from '../components/RadioSelection';
import ToppingRow from '../components/ToppingRow';
import { useCartContext } from '../context/useCartContext';
import useDocumentTitle from '../hooks/useDocumentTitle';
import { Base, Dough, Size, sortedToppings, Topping } from '../models/type/type';
import assetsUrl from '../utils/assets';
import { idGenerator } from '../utils/Id';
import CartPage from './CartPage';

export default function ConfigurePage() {
  const { addPizza } = useCartContext();
  const navigate = useNavigate();
  const [toppings, setToppings] = useState<Topping[]>([]);
  const [dough, setDough] = useState<Dough>(Dough.THIN);
  const [size, setSize] = useState<Size>(Size.MEDIUM);
  const [base, setBase] = useState<Base | null>(null);

  // Hook custom avec un useEffect.
  useDocumentTitle('Configurer ma pizza');

  function addTopping(topping: Topping): void {
    setToppings([...toppings, topping]);
  }

  function removeTopping(topping: Topping): void {
    setToppings(toppings.filter((t) => t !== topping));
  }

  function toggleTopping(topping: Topping) {
    return toppings.includes(topping) ? removeTopping(topping) : addTopping(topping);
  }
  function addToCart(): void {
    if (!window.confirm('Voulez-vous ajouter cette pizza à votre panier ?')) {
      return;
    }

    addPizza({
      id: idGenerator(),
      dough,
      size,
      base,
      toppings,
    });
    navigate(CartPage.path);
  }

  return (
    <div>
      <div id="configure" className={'grid grid-cols-2'}>
        <div id="parameters">
          <div id="topping" className={'m-4'}>
            <ul className={'grid grid-cols-3'}>
              {Object.values(Topping).map((topping) => (
                <li key={topping}>
                  <ToppingRow
                    isSelected={toppings.includes(topping)}
                    topping={topping}
                    toggleTopping={toggleTopping}
                  />
                </li>
              ))}
            </ul>
          </div>
          <div id="radioChoices" className={'grid grid-cols-3'}>
            <div id="size">
              {Object.values(Size).map((s) => (
                <RadioSelection key={s} name={'size'} value={s} item={size} setItem={setSize} />
              ))}
            </div>
            <div id="dough">
              {Object.values(Dough).map((d) => (
                <RadioSelection key={d} name={'dough'} value={d} item={dough} setItem={setDough} />
              ))}
            </div>
            <div id="base">
              {Object.values(Base).map((b) => (
                <RadioSelection key={b} name={'base'} value={b} item={base} setItem={setBase} />
              ))}
            </div>
          </div>
        </div>
        <div id="preview" className={'m-4 relative w-[400px] h-[400px]'}>
          <img src={emptyBase} className={'w-full h-full absolute'} />
          {base && <img src={assetsUrl(`base/${base}.png`)} className={'w-full h-full absolute'} />}
          {sortedToppings.map(
            (topping) =>
              toppings.includes(topping) && (
                <img
                  key={topping}
                  src={assetsUrl(`toppings/preview/${topping}.png`)}
                  className={'w-full h-full absolute'}
                />
              ),
          )}
        </div>
      </div>
      <button onClick={addToCart}>Ajouter au panier</button>
      <Link to={CartPage.path}>Voir le panier</Link>
    </div>
  );
}
ConfigurePage.path = '/';
