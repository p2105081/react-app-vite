import { Link } from 'react-router-dom';

import { useCartContext } from '../context/useCartContext';
import useDocumentTitle from '../hooks/useDocumentTitle';
import { Pizza } from '../models/type/type';
import trans from '../translations';
import ConfigurePage from './ConfigurePage';

function generateLabel(pizza: Pizza): string {
  const basePizza = pizza.base ? `Pizza base ${trans(`base.${pizza.base}`)},` : 'No Pizza base,';
  return `${basePizza} taille ${trans(`size.${pizza.size}`)}`;
}

function CartRow({ pizza, remove }: CartRowProps) {
  return (
    <>
      {/*todo: images */}
      <span>{generateLabel(pizza)}</span>
      <button>Modifier</button>
      <button onClick={() => remove(pizza)}>Supprimer</button>
    </>
  );
}

export default function CartPage() {
  const { products, removePizza } = useCartContext();

  // Hook custom avec un useEffect.
  useDocumentTitle('Mon panier');

  return (
    <div>
      <ul>
        {products.map((pizza) => (
          <li key={pizza.id}>
            <CartRow pizza={pizza} remove={removePizza} />
          </li>
        ))}
      </ul>
      <Link to={ConfigurePage.path}>Ajouter une nouvelle pizza</Link>
    </div>
  );
}

CartPage.path = '/cart';

interface CartRowProps {
  pizza: Pizza;
  remove: (p: Pizza) => void;
}
