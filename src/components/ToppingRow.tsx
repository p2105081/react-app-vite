import clsx from 'clsx';

import { Topping } from '../models/type/type';

interface ToppingRowProps {
  isSelected: boolean;
  topping: Topping;
  toggleTopping: (t: Topping) => void;
}

export default function ToppingRow({ isSelected, topping, toggleTopping }: ToppingRowProps) {
  return (
    <>
      <button
        className={clsx('m-2', {
          'bg-blue-400 border-white': isSelected,
        })}
        onClick={() => toggleTopping(topping)}
      >
        {topping}
      </button>
    </>
  );
}
